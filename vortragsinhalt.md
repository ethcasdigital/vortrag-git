# Git und Git als Cloudplattform
## Versionsverwaltung
### Allgemeines
Eine Versionsverwaltung ist ein System, das zur Erfassung von Änderungen an Dokumenten oder Dateien verwendet wird. Alle Versionen werden in einem Archiv mit Zeitstempel und Benutzerkennung gesichert und können später wiederhergestellt werden. Versionsverwaltungssysteme werden typischerweise in der Softwareentwicklung eingesetzt, um Quelltexte zu verwalten. Versionsverwaltung kommt auch bei Büroanwendungen oder Content-Management-Systemen zum Einsatz.

Ein Beispiel ist auch die Wikipedia, hier erzeugt die Software nach jeder Änderung eines Artikels eine neue Version. Alle Versionen bilden in Wikipedia eine Kette, in der die letzte Version gültig ist; es sind keine Varianten vorgesehen. Da zu jedem Versionswechsel die grundlegenden Angaben wie Verfasser und Uhrzeit festgehalten werden, kann genau nachvollzogen werden, wer wann was geändert hat. Bei Bedarf – beispielsweise bei versehentlichen Änderungen – kann man zu einer früheren Version zurückkehren.

### Hauptaufgaben
- __Protokollierungen__ der Änderungen: Es kann jederzeit nachvollzogen werden, wer wann was geändert hat.
- __Wiederherstellung__ von alten Ständen einzelner Dateien: Somit können versehentliche Änderungen jederzeit wieder rückgängig gemacht werden.
- __Archivierung__ der einzelnen Stände eines Projektes: Dadurch ist es jederzeit möglich, auf alle Versionen zuzugreifen.
- Koordinierung des __gemeinsamen Zugriffs__ von mehreren Entwicklern auf die Dateien.
- Gleichzeitige Entwicklung __mehrerer Entwicklungszweige__ (engl. Branch) eines Projektes, was nicht mit der Abspaltung eines anderen Projekts (engl. Fork) verwechselt werden darf.

### Terminologie
Ein __Branch__, zu deutsch Zweig, ist eine Verzweigung von einer anderen Version, so dass unterschiedliche Versionen parallel im selben Projekt weiterentwickelt werden können. Änderungen können dabei von einem Branch auch wieder in einen anderen einfließen, das als Merging, zu deutsch verschmelzen, bezeichnet wird. Oft wird der Hauptentwicklungszweig als Trunk (z. B. bei Subversion) oder __Master__ (z. B. bei Git) bezeichnet. Branches können zum Beispiel für neue Hauptversionen einer Software erstellt werden oder für Entwicklungszweige für unterschiedliche Betriebssysteme oder aber auch, um experimentelle Versionen zu haben. Wird ein Zweig in einer neuen, unabhängigen Versionsverwaltung erstellt, spricht man von einem __Fork__.

### Funktionsweise
### Lokale Versionsverwaltung
Bei der lokalen Versionsverwaltung wird oft nur eine einzige Datei versioniert, diese Variante wurde mit Werkzeugen wie SCCS und RCS umgesetzt. Sie findet auch heute noch Verwendung in Büroanwendungen, die Versionen eines Dokumentes in der Datei des Dokuments selbst speichern. Auch in technischen Zeichnungen werden Versionen zum Beispiel durch einen Änderungsindex verwaltet.

#### Zentrale Versionsverwaltung
Diese Art ist als Client-Server-System aufgebaut, sodass der Zugriff auf ein Repository auch über Netzwerk erfolgen kann. Durch eine Rechteverwaltung wird dafür gesorgt, dass nur berechtigte Personen neue Versionen in das Archiv legen können. Die Versionsgeschichte ist hierbei nur im Repository vorhanden. Dieses Konzept wurde vom Open-Source-Projekt Concurrent Versions System (CVS) populär gemacht, mit Subversion (SVN) neu implementiert und von vielen kommerziellen Anbietern verwendet.

#### Verteilte Versionsverwaltung
Die verteilte Versionsverwaltung (DVCS, distributed VCS) verwendet kein zentrales Repository mehr. Jeder, der an dem verwalteten Projekt arbeitet, hat sein eigenes Repository und kann dieses mit jedem beliebigen anderen Repository abgleichen. Die Versionsgeschichte ist dadurch genauso verteilt. Änderungen können lokal verfolgt werden, ohne eine Verbindung zu einem Server aufbauen zu müssen.

Im Gegensatz zur zentralen Versionsverwaltung kommt es nicht zu einem Konflikt, wenn mehrere Benutzer dieselbe Version einer Datei ändern. Die sich widersprechenden Versionen existieren zunächst parallel und können weiter geändert werden. Sie können später in eine neue Version zusammengeführt werden. Dadurch entsteht ein gerichteter azyklischer Graph (Polyhierarchie) anstatt einer Kette von Versionen. In der Praxis werden bei der Verwendung in der Softwareentwicklung meist einzelne Features oder Gruppen von Features in separaten Versionen entwickelt und diese bei größeren Projekten von Personen mit einer Integrator-Rolle überprüft und zusammengeführt.

Systembedingt bieten verteilte Versionsverwaltungen keine Locks. Da wegen der höheren Zugriffsgeschwindigkeit die Granularität der gespeicherten Änderungen viel kleiner sein kann, können sie sehr leistungsfähige, weitgehend automatische Merge-Mechanismen zur Verfügung stellen.

Eine Unterart der Versionsverwaltung bieten einfachere Patchverwaltungssysteme, die Änderungen nur in eine Richtung in Produktivsysteme einspeisen.

Obwohl konzeptionell nicht unbedingt notwendig, existiert in verteilten Versionsverwaltungsszenarien üblicherweise ein offizielles Repository. Das offizielle Repository wird von neuen Projektbeteiligten zu Beginn ihrer Arbeit geklont, d. h. auf das lokale System kopiert. 

##  Git in der Cloud
- https://www.gitlab.com
- https://www.github.com

